#!/usr/bin/env bash
if docker run --name datacube \
            -d \
            -p 8000:4200 \
            idocias/datacube:latest . 
then 
    sleep 15
    echo "Please launch : http://localhost:8000/DataCube"
fi