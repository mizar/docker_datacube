
# DataCube -  Docker image

This image contains the [DataCube](https://github.com/MizarWeb/DataCube) apps that helps to view cubes of spectral data.
> **WARNING**: Without a server answering to the DataCube [API](https://github.com/MizarWeb/DataCube/tree/master/SERVER). This app will not be able to load any file. If you want to run DataCube with a server please use docker image [idocias/datacube-and-server](https://hub.docker.com/r/idocias/datacube-and-server).


## Usage

Use the script [run.sh](https://git.ias.u-psud.fr/mizar/docker_datacube/blob/master/run.sh)  to run a new container from this image.

You can then access the app from your navigator at  http://localhost:8000/demo/DataCube and enjoy the CNES DataCube interface.


## Logging
To access the container's logs do :

```bash
docker logs datacube
```
## Cleanup

To discard your container run the script [clean.sh](https://git.ias.u-psud.fr/mizar/docker_datacube/blob/master/clean.sh).
